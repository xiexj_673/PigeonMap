//
//  XXMapViewModel.swift
//  PigeonMap
//
//  Created by 放学跟鸽走 on 2017/5/11.
//  Copyright © 2017年 放学跟鸽走. All rights reserved.
//

import Foundation
import YYModel

class XXMapViewModel{
    
    /// 从数据中获取数据
    ///
    /// - Returns:
    func getNewsFromDB()->[XSquareRetweetNews]{
    
        let array = XXSQLiteManager.shared.getAllSquareNews()
        
        guard let tempList = NSArray.yy_modelArray(with: XSquareRetweetNews.self, json: array) as? [XSquareRetweetNews]
            else{
                return [XSquareRetweetNews]()
        }
        
        return tempList
    }
    
    
}

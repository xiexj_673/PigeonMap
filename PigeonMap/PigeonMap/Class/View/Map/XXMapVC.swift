//
//  XXMapVC.swift
//  PigeonMap
//
//  Created by 放学跟鸽走 on 2017/5/11.
//  Copyright © 2017年 放学跟鸽走. All rights reserved.
//

import UIKit

class XXMapVC: UIViewController,MAMapViewDelegate {
    lazy var vm = XXMapViewModel()
    var list:[XSquareRetweetNews]?
    lazy var mapView:MAMapView = MAMapView(frame: self.view.bounds)
    
    

    override func viewDidLoad() {
        print("viewdidLoad")
        super.viewDidLoad()
        list = vm.getNewsFromDB()
        
        initMap()
        
        // Do any additional setup after loading the view.
    }
    
    func initMap(){
        
        mapView.delegate = self
        ///如果您需要进入地图就显示定位小蓝点，则需要下面两行代码
        //定位点相关
        mapView.showsUserLocation = true;
        mapView.userTrackingMode = .follow;
        let r = MAUserLocationRepresentation()
        r.showsAccuracyRing = true
        mapView.update(r)
        //指南针控件
        mapView.showsCompass = false
//        mapView.compassOrigin = CGPoint(x: 10, y: UIScreen.cz_screenHeight()-50)
        
        mapView.showsScale = false  //设置成NO表示不显示比例尺；YES表示显示比例尺
//        mapView.scaleOrigin = CGPoint(x: 10, y: UIScreen.cz_screenHeight()-100) //设置比例尺位置
        
        mapView.isRotateEnabled = false//旋转手势
        mapView.isRotateCameraEnabled = false//倾斜手势
        
        

        self.view.addSubview(mapView)
    }
}
extension XXMapVC{
    
   
}
